//llamar lo que vamos a probar
const sumar = require("../index");
//pruebas de software con assert
const assert = require("assert");

describe("Probar la suma de dos numeros", ()=>{
    //afirmar que cinco mas cinco es 10
    it('Cinco mas cinco es 10', ()=>{
        //siempre se debe pasar el valor esperado
        assert.equal(10, sumar(5,5));
    });
    it('Afirmar que cinco mas cinco no son ciencuenta y cinco', ()=>{
        //siempre se debe pasar el valor esperado
        assert.notEqual("55", sumar(5,5));
    });
});